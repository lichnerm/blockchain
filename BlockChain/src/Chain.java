
import java.security.PublicKey;
import java.util.LinkedList;
import java.util.List;

public class Chain {

    private static Chain instance = new Chain();
    private List<Block> chain;

    private Chain() {
        this.chain = new LinkedList<>();
        this.chain.add(new Block(null, new Transaction(100, null, null)));
    }

    @Override
    public String toString() {
        String output = "Chain{\nchain=";
        for (Block block : this.chain) {
            output += "\n\n" + block;
        }
        output += "\n}";
        return output;
    }

    public Block lastBlock() {
        return this.chain.get(this.chain.size() - 1);
    }

    public void addBlock(Transaction transaction, PublicKey senderPublicKey, String signature) {
        String hash = CryptoUtils.getHash(transaction.toString(), CryptoUtils.SHA_256);
        boolean isValid = CryptoUtils.verify(hash, signature, senderPublicKey);
        if (isValid) {
            Block block = new Block(lastBlock().getHash(), transaction);
            mine(block.getNonce());
            this.chain.add(block);
        }
    }

    private int mine(int nonce) {
        int solution = 1;
        System.out.println("mining...");

        while(true) {
            String attemp = CryptoUtils.getHash(String.valueOf(nonce + solution), CryptoUtils.MD5);
            if (attemp.startsWith("0000")) {
                System.out.println("solution: " + solution);
                return solution;
            }
            solution += 1;
        }
    }

    public static Chain getInstance() {
        return instance;
    }

}
