import java.security.PublicKey;

public class Transaction {

    private double amount;
    private PublicKey payer;
    private PublicKey payee;

    public Transaction(double amount, PublicKey payer, PublicKey payee) {
        this.amount = amount;
        this.payer = payer;
        this.payee = payee;
    }

    @Override
    public String toString() {
        if (payer != null && payee != null) {
            return "\nTransaction{" +
                    "\namount=" + amount +
                    ", \npayer='" + payer + '\'' +
                    ", \npayee='" + payee + '\'' +
                    '}';
        } else {
            return "\nTransaction{" +
                    "\namount=" + amount +
                    ", \npayer=null\''" +
                    ", \npayee=null\''" +
                    '}';
        }
    }
}
