public class Blockchain {

    public Blockchain() {
        Wallet satoshi = new Wallet();
        Wallet bob = new Wallet();
        Wallet alice = new Wallet();

        satoshi.sendMoney(50, bob.getPublicKey());
        bob.sendMoney(23, alice.getPublicKey());
        alice.sendMoney(5, bob.getPublicKey());

        //System.out.println(Chain.getInstance().toString());
    }
}
