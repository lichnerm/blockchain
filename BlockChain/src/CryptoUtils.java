import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;

public abstract class CryptoUtils {

    private static final String SHA_256_WITH_RSA = "SHA256withRSA";
    private static final String RSA = "RSA";
    public static final String SHA_256 = "SHA-256";
    public static final String MD5 = "MD5";

    public static String getHash(String str, String type) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(type);
            byte[] arr = digest.digest(str.getBytes(StandardCharsets.UTF_8));
            return CryptoUtils.bytesToHex(arr);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String sign(String plainText, PrivateKey key) {
        Signature privateSignature = null;
        try {
            privateSignature = Signature.getInstance(SHA_256_WITH_RSA);
            privateSignature.initSign(key);
            privateSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

            byte[] signature = privateSignature.sign();

            return Base64.getEncoder().encodeToString(signature);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean verify(String plainText, String signature, PublicKey publicKey) {
        try {
            Signature publicSignature = Signature.getInstance(SHA_256_WITH_RSA);
            publicSignature.initVerify(publicKey);
            publicSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

            byte[] signatureBytes = Base64.getDecoder().decode(signature);

            return publicSignature.verify(signatureBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static KeyPair generateKeyPair(int keySize) {
        KeyPairGenerator keyGen = null;
        try {
            keyGen = KeyPairGenerator.getInstance(RSA);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        keyGen.initialize(keySize);
        return keyGen.generateKeyPair();
    }
}
