
import java.security.*;

public class Wallet {

    private KeyPair pair;

    public Wallet() {
        this.pair = CryptoUtils.generateKeyPair(2048);
    }

    public PublicKey getPublicKey() {
        return this.pair.getPublic();
    }

    public void sendMoney(double amount, PublicKey payeePublicKey) {
        Transaction transaction = new Transaction(amount, this.pair.getPublic(), payeePublicKey);

        String hash = CryptoUtils.getHash(transaction.toString(), CryptoUtils.SHA_256);
        String signature = null;
        try {
            signature = CryptoUtils.sign(hash, this.pair.getPrivate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Chain.getInstance().addBlock(transaction, this.pair.getPublic(), signature);
    }
}
