import java.time.LocalDateTime;

public class Block { //can contain multiple transaction

    private String prevHash;
    private Transaction transaction;
    private LocalDateTime timeStamp;

    private int nonce = (int) Math.round(Math.random() * 999999);

    public Block(String prevHash, Transaction transaction) {
        this.prevHash = prevHash;
        this.transaction = transaction;
        this.timeStamp = LocalDateTime.now();
    }

    public String getHash() {
        return CryptoUtils.getHash(toString(), CryptoUtils.SHA_256);
    }

    public int getNonce() {
        return nonce;
    }

    @Override
    public String toString() {
        return "Block{" +
                "prevHash='" + prevHash + '\'' +
                ", transaction=" + transaction +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
